﻿//Lenaの画像は画像認識分野でよく使われるモノの白黒の方をソースと同じディレクトリにおいてください
//もちろん他の画像でも構いません
//opencv3.4とEigenをインストールした環境で実行してください

#include <string> 
#include "pch.h"
#include <iostream>
#include <opencv2/opencv.hpp>
#include <algorithm>
#include <Eigen/Core>
#include <Eigen/SVD>
#include <random>


using namespace cv;
using namespace std;
using namespace Eigen;

const double v_rate = 0.01;//核ノルム最小化定数0.01~0.05
const int p = 20; //画像の欠損率　（０～１００）
random_device rnd;
mt19937 mt(rnd());
uniform_int_distribution<> rand100(0, 99);

auto cv1_to_eigen(Mat m) {
	MatrixXd e(m.rows, m.cols);

	for (int rows = 0; rows < m.rows; rows++) {
		for (int cols = 0; cols < m.cols; cols++) {
			e(rows, cols) = m.at<uchar>(rows, cols);
		}
	}
	return e;
}

Mat eigen_to_cv1(MatrixXd e) {

	Mat m(e.rows(),e.cols(),CV_8UC1);

	for (int rows = 0; rows < e.rows(); rows++) {
		for (int cols = 0; cols < e.cols(); cols++) {
			m.at<uchar>(rows, cols) =(uchar)e(rows, cols)  ;
		}
	}
	return m;
}



auto min_S(MatrixXd S) {
	double s_max = S.maxCoeff();
	double v = s_max * v_rate;
	for (int row = 0; row < S.rows(); row++) {
		for (int col = 0; col < S.cols(); col++) {
			S(row, col) = max((S(row, col) - v), 0.0);
		}
	}
	return S;

}

Mat repare(MatrixXd E, MatrixXd Om,MatrixXd Om_In,int roop_count) {
	for (int n = 0; n < roop_count; n++) {
		BDCSVD <MatrixXd> svd = E.bdcSvd(ComputeThinU | ComputeThinV);
		MatrixXd U = svd.matrixU();
		MatrixXd S = svd.singularValues().asDiagonal();
		MatrixXd V = svd.matrixV();
		MatrixXd E2(E.rows(), E.cols());
		E2 = U * min_S(S)* V.transpose();
		E = E2.array()*Om.array() + E.array()*Om_In.array();
	}
	return eigen_to_cv1(E);
}

int main()
{
	const string filename = "LENNA.jpeg";
	Mat img = imread(filename, CV_8UC1);
	imshow("original", img);
	MatrixXd E(img.rows, img.cols);
	E= cv1_to_eigen(img);
	MatrixXd Om(img.rows, img.cols);//欠損個所が１、正常個所が０
	MatrixXd Om_In(img.rows, img.cols);//欠損個所が０、正常個所が１
	for (int row = 0; row < img.rows; row++) {
		for (int col = 0; col < img.cols; col++) {
			if (rand100(mt) < p) {
				Om(row, col) = 1;
				Om_In(row, col) = 0;
			}
			else {
				Om(row, col) = 0;
				Om_In(row, col) = 1;
			}
		}
	}

	Mat img_miss = eigen_to_cv1( E.array()*Om_In.array());
	imshow("miss", img_miss);
	Mat img_remake = repare(E,Om,Om_In,20);
	imshow("remake", img_remake);


	waitKey(0);

}